#if !defined(instructions_IRCsq4)
#define instructions_IRCsq4
// #include "../AST/ast.h"
#include "builtins.h"
//Instructions

#define VAR auto
#define FUN auto
#define FOR for
#define WHILE while
#define CLASS class
#define IN :
#define IF if
#define ELIF else if
#define ELSE else
#define SWITCH switch
#define CASE case:
#define BREAK break
#define GROUP class
#define ignore 

#endif // instructions_IRCsq4
